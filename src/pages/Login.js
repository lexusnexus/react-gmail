import React,{useState}  from 'react';
import './../App.css';
import GoogleLogin from 'react-google-login';
import gmailLogo from './../images/Gmail-Logo.png';

export default function Login() {
  
  const [loginData, setLoginData] = useState(
    localStorage.getItem('loginData')
    ? JSON.parse(localStorage.getItem('loginData'))
      : null
  );
  const [token, setToken] = useState(null);
  
  const handleFailure = (result) => {
    alert(result);
  };
  
  const handleLogin = async (googleData) => {
    const res = await fetch('/api/google-login', {
      method: 'POST',
      body: JSON.stringify({
        token: googleData.tokenId,
      }),
      headers: {
        'Content-Type': 'application/json',
      },
    });
    
    const data = await res.json();
    
    setLoginData(data);
    localStorage.setItem("mailPic", data.picture);
    localStorage.setItem('loginData', JSON.stringify(data));
    
    localStorage.setItem("access_token", googleData.accessToken);
    setToken(googleData.accessToken);
  };
  const refresh = () => {
    window.location.reload(false);
  }
  
  return (
    ( token !==null || localStorage.getItem('access_token') !==null )
    ? refresh()   
      : <div>
          <div className="App">
            <header className="App-header">
              <img src={gmailLogo} alt="" height="200"/>
              <h1>React Gmail Clone App</h1>
              <div>
                <GoogleLogin
                  clientId={process.env.REACT_APP_GOOGLE_CLIENT_ID}
                  buttonText="Log in with Google"
                  onSuccess={handleLogin}
                  onFailure={handleFailure}
                  cookiePolicy={'single_host_origin'}
                  scope="email profile https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile openid https://mail.google.com/"
                  >
                </GoogleLogin>
                
              </div>
            </header>
          </div>
        </div>
  )
}