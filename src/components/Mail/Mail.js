import React from "react";
import "./Mail.css";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import MoveToInboxIcon from "@material-ui/icons/MoveToInbox";
import ErrorIcon from "@material-ui/icons/Error";
import DeleteIcon from "@material-ui/icons/Delete";
import EmailIcon from "@material-ui/icons/Email";
import WatchLaterIcon from "@material-ui/icons/WatchLater";
import CheckCircleIcon from "@material-ui/icons/CheckCircle";
import LabelImportantIcon from "@material-ui/icons/LabelImportant";
import MoreVertIcon from "@material-ui/icons/MoreVert";
import UnfoldMoreIcon from "@material-ui/icons/UnfoldMore";
import PrintIcon from "@material-ui/icons/Print";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import { IconButton } from "@material-ui/core";
import { useNavigate } from "react-router-dom";
import {useLocation} from 'react-router-dom';
import parse from 'html-react-parser';
function Mail() {
  
  const navigate = useNavigate();
  const location = useLocation();
  let bodyMail = location.state.body;
  
  function decodeBase64(base64) {
    const text = atob(base64);
    const length = text.length;
    const bytes = new Uint8Array(length);
    for (let i = 0; i < length; i++) {
      bytes[i] = text.charCodeAt(i);
    }
    const decoder = new TextDecoder(); // default is utf-8
    return decoder.decode(bytes);
  }
  
  let bodyDecoded = decodeBase64(bodyMail.replace(/-/g, '+').replace(/_/g, '/').replace(/(\r\n|\n|\r)/gm, ""))
  
  function convertToPlain(html){
    var tempDivElement = document.createElement("div"); //show html text
    tempDivElement.textContent = html;
    return tempDivElement.textContent || tempDivElement.innerText || "";
  }
  
  
  bodyDecoded = convertToPlain(bodyDecoded);
  bodyDecoded = parse(bodyDecoded);
  return (
    <div className="mail">
      <div className="mail-tools">
        <div className="mail-toolsLeft">
          <IconButton onClick={() => navigate("/")}>
            <ArrowBackIcon />
          </IconButton>
          
          <IconButton>
            <MoveToInboxIcon />
          </IconButton>
          
          <IconButton>
            <ErrorIcon />
          </IconButton>
          
          <IconButton>
            <DeleteIcon />
          </IconButton>
          
          <IconButton>
            <EmailIcon />
          </IconButton>
          
          <IconButton>
            <WatchLaterIcon />
          </IconButton>
          
          <IconButton>
            <CheckCircleIcon />
          </IconButton>
          
          <IconButton>
            <LabelImportantIcon />
          </IconButton>
          
          <IconButton>
            <MoreVertIcon />
          </IconButton>
        </div>
        <div className="mail-toolsRight">
          <IconButton>
            <UnfoldMoreIcon />
          </IconButton>
          
          <IconButton>
            <PrintIcon />
          </IconButton>
          
          <IconButton>
            <ExitToAppIcon />
          </IconButton>
        </div>
      </div>
      <div className="mail-body">
        <div className="mail-bodyHeader">
          <div className="mail-subject">
            <h5>{location.state.subject}</h5>
            <LabelImportantIcon className="mail-important" />
          </div>
          
          <p className="mail-from">{location.state.name}</p>
          
          <p className="mail-time">{location.state.time}</p>
          
        </div>
        
        <div>
          <p>{bodyDecoded}</p>
        </div>
      </div>
    </div>
  );
}

export default Mail;
